﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warcaby_online
{
    class Board
    {
        //int[,] board = new int[8, 8];
        public Draft[,] board;

        public Board()
        {
            create_board();
        }
       public Board(Draft[,] board)
        {
            this.board = board;
           
        }

        public void create_board()
        {
            int[,] board = new int[8, 8];
            create_onePlayer();
            create_twoPlayer();
        }
        public Draft this[int y, int x]
        {
            get
            {
                return board[y, x];
            }
            set
            {
                board[y, x] = value;
            }
        }
        public void create_onePlayer()
        {
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    if ((y + x) % 2 != 0)
                    {
                        board[y, x] = new Draft(x, y, Player.PlayerOne);
                    }
                }
            }
        }
        public void create_twoPlayer()
        {
            for (int y = 5; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    if ((y + x) % 2 != 0)
                    {
                       board[y, x] = new Draft(x, y, Player.PlayerTwo);
                    }
                }
            }
        }

        public Draft[,] szachownica
        {
            get
            {
                return board;
            }
        }
        public void move(int oldX, int oldY, int newX, int newY)
        {
            board[newY, newX] = board[oldY, oldX];
            board[newY, newX].X = newX;
            board[newY, newX].Y = newY;
            board[oldY, oldX] = null;
        }

        public void show()
        {
            Console.Write(board[1,1]);
        }
        public void show_Board()
        {
            for (int y = 0; y < 8; y++)
            {
                Console.Write('\n');
                for (int x = 0; x < 8; x++)
                {
                    Console.Write(board[y,x]);
                }
            }
        }
    }
}
